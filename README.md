# memorylane

## Installation Linux

Install ncurses with unicode support, as shown (for ubuntu)

```console
sudo apt install libncurses-dev libncursesw5-dev
```

compile it with the following command -

```console
g++ main.cpp -lncursesw
```

run `./a.out` produced in the same directory

## Installation Windows

If you have WSL then just Shift+RightClick in the *memorylane* folder and click on *Open Linux Shell here*. Follow instructions for Linux above.

> **Caution Notes**:
> *It wasn't developed keeping Windows in mind*
>
> Try running in powershell instead of command prompt.
>
> Resize the powershell window when needed.
>
> Press ENTER when asked to "press a key"
>
> The emojis will not display in Windows


Skip **step 1** if the following command (in command prompt or powershell) produces some output as shown below -

```console
g++ --version
```
OUTPUT (something similar)-

```console
g++ (MinGW.org GCC-8.2.0-3) 8.2.0
Copyright (C) 2018 Free Software Foundation, Inc.
```

1. See [this answer](https://stackoverflow.com/questions/11365850/run-c-in-command-prompt-windows#answer-55112828) for installing MingW and adding it to PATH in windows.

2. Download PDCurses from - [pdcurses](https://sourceforge.net/projects/pdcurses/files/pdcurses/3.4/pdc34dllw.zip/download)

3. See the section on **Adding PDCurses to MingW** [here](https://superuser.com/questions/1121913/how-do-we-install-new-libraries-for-c-mingw#answer-1122345) for installing ncurses library for windows

4. In the memorylane folder, open `memorylaneheaders.h` in text editor and change the line `#include<ncurses.h>` to `#include<curses.h>`

5. Open powershell window (Shift+RightClick) in the folder *memorylane*

5. Type `g++ main.cpp -lpdcurses -o a.exe`

6. Run by typing `.\a.out`

---

![screenshot](screenshot.png)


