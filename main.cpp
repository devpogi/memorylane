#include "memorylaneheaders.h"

vector<MessageContainer> content;
WINDOW *lwin, *rwin;

void printmenu(WINDOW *win, Menu *menu)
{
    int maxRows = getmaxy(win);
    int noOfItems = maxRows - 4;
    int totalSets = (menu->items.size() / noOfItems) + 1;
    int startIndex;
    int endIndex;
    startIndex = (menu->setIndex - 1) * (noOfItems);
    if (menu->setIndex < totalSets)
    {
        endIndex = startIndex + noOfItems;
        int itemsRem = menu->items.size() - endIndex;
        mvwprintw(win, maxRows - 2, 1, "DOWN | Remaining: %d/%d", itemsRem, menu->items.size());
    }
    else if (menu->setIndex == totalSets) //last set
    {
        endIndex = menu->items.size();
        int itemsRem = menu->items.size() - endIndex;
        mvwprintw(win, maxRows - 2, 1, " | Remaining: %d/%d", itemsRem, menu->items.size());
    }

    int y = 2;
    if (menu->setIndex != 1)
    {
        mvwprintw(win, 1, 1, "UP");
    }
    for (int i = startIndex, j = 0; i < endIndex; i++, j++)
    {
        if (menu->highlightIndex == y + j)
        {
            wattron(win, A_REVERSE);
            mvwprintw(win, y + j, 1, (menu->items[i]).c_str());
            wattroff(win, A_REVERSE);
        }
        else
        {
            mvwprintw(win, y + j, 1, (menu->items[i]).c_str());
        }
    }
    wrefresh(win);
}

int printMenuActions(WINDOW *win, Menu *menu)
{
    keypad(win, true);
    wborder(win, 0, 0, 0, 0, '*', '*', '*', '*');
    wrefresh(win);
    int maxRows = getmaxy(win);
    int bottomRow = maxRows - 2; // the row which has press down/up arrow
    int noOfItems = maxRows - 4;
    int lastSet = (menu->items.size() / noOfItems) + 1;
    if (menu->items.size() % noOfItems == 0)
    {
        lastSet = lastSet - 1;
    }
    int lastIndex = 1 + (menu->items.size() - (lastSet - 1) * (noOfItems));
    int ch = ' ';
    ch = wgetch(win);
    while (ch != 'q' && ch != KEY_RIGHT)
    {
        switch (ch)
        {
        case KEY_UP:
            if (menu->highlightIndex == 2)
            {
                if (menu->setIndex == 1)
                {
                    // do nothing, no entries above
                }
                else
                {
                    menu->setIndex--;
                    menu->highlightIndex = bottomRow - 1;
                }
            }
            else
            {
                menu->highlightIndex--;
            }
            break;
        case KEY_DOWN:
            if (menu->setIndex != lastSet)
            {
                if (menu->highlightIndex == bottomRow - 1)
                {
                    menu->highlightIndex = 2;
                    menu->setIndex++;
                }
                else
                {
                    menu->highlightIndex++;
                }
            }
            else
            {
                if (menu->highlightIndex == lastIndex)
                {

                    // do nothing, no entries beyond
                }
                else
                {
                    menu->highlightIndex++;
                }
            }
            break;
        }
        wclear(win);
        wborder(win, 0, 0, 0, 0, '*', '*', '*', '*');
        wrefresh(win);
        printmenu(win, menu);
        int realIndex = (menu->setIndex - 1) * (noOfItems) + menu->highlightIndex - 2;
        string dateAndMessage = content[realIndex].datefield + "\n" + content[realIndex].message;
        rightWindowContent(rwin, dateAndMessage);
        ch = wgetch(win);
    }
    box(win, 0, 0);
    wrefresh(win);
    keypad(win, false);
    return ch;
}

void rightWindowContent(WINDOW *win, string verylong)
{
    wclear(win);
    box(win, 0, 0);
    int maxY, maxX;
    getmaxyx(win, maxY, maxX);
    string msg = "PAGING FOR THIS WINDOW IS UNIMPLEMENTED :(";
    mvwprintw(win, maxY - 2, 1, msg.c_str());

    vector<string> splitstring;
    for (int j = 0; j < verylong.size(); j++)
    {
        if (verylong[j] == '\n')
        {
            splitstring.push_back(verylong.substr(0, j));
            verylong = verylong.substr(j + 1, verylong.size() - j - 1);
            j = 0;
        }
    }
    for (int j = 0; j < splitstring.size(); j++)
    {
        mvwprintw(win, 1 + j, 1, splitstring[j].c_str());
    }

    wrefresh(win);
}

// int rightWindowActions(WINDOW *win, Content* above, Content* below){
// wborder(win, 0, 0, 0, 0, '*', '*', '*', '*');
// wrefresh(win);
// keypad(win, true);
// int ch = ' ';
// ch = wgetch(win);
// while(ch!='q' && ch!=KEY_LEFT){
//     switch (ch)
//     {
//     case KEY_DOWN:
//         if(below->lines!=0){
//             rightWindowContent(win, below);
//         }
//         break;
//     case KEY_UP:
//         if(above->lines!=0){
//             rightWindowContent(win, above);
//         }
//         break;
//     }
//     ch = wgetch(win);
// }
// box(win, 0, 0);
// wrefresh(win);
// keypad(win, false);
// return ch;
// }

vector<MessageContainer> getmessage(string filename)
{
    fstream fp;
    fp.open(filename, ios::in);
    vector<MessageContainer> msgs;
    MessageContainer entry;
    entry.datefield = "";
    if (!fp)
    {
        printw("Error opening %s file", filename);
        getch();
        exit(EXIT_FAILURE);
    }
    else
    {
        while (!fp.eof())
        {
            string c;
            getline(fp, c);

            if (c[0] == '[')
            {
                if (entry.datefield != "")
                {
                    msgs.push_back(entry);
                    entry.datefield = "";
                    entry.message = "";
                }
                entry.datefield.append(c);
            }
            else
            {
                entry.message.append(c + "\n");
            }
        }
        msgs.push_back(entry);
        entry.datefield = "";
        entry.message = "";
    }
    return msgs;
}

void improveDateFormat(vector<MessageContainer> *content)
{
    vector<MessageContainer>::iterator it;
    for (it = content->begin(); it != content->end(); it++)
    {
        //improving month
        string dateFormat = (*it).datefield;
        int monthNo = dateFormat[1] - '0';
        if (dateFormat[2] != '/')
        {
            monthNo = 10 * monthNo + (dateFormat[2] - '0');
        }
        string monthName = months[monthNo - 1];

        string prefixdate = string(1, dateFormat[0]);
        string suffixdate = dateFormat.substr(2, dateFormat.size() - 2);
        if (monthNo > 9)
        {
            suffixdate = dateFormat.substr(3, dateFormat.size() - 3);
        }
        dateFormat = prefixdate + monthName + suffixdate;
        (*it).datefield = dateFormat;
        // improving date
        dateFormat = (*it).datefield;
        string dd = "";
        if (dateFormat[6] == '/')
        {
            dd.append("0");
        }
        dd.append(string(1, dateFormat[5]));
        if (dd.size() != 2)
        {
            dd.append(string(1, dateFormat[6]));
        }
        prefixdate = dateFormat.substr(0, 5);
        suffixdate = dateFormat.substr(7, dateFormat.size() - 7);
        if (dateFormat[6] == '/')
        {
            suffixdate = dateFormat.substr(6, dateFormat.size() - 6);
        }
        dateFormat = prefixdate + dd + suffixdate;
        (*it).datefield = dateFormat;
    }
}

string formattedfile(string infile, int lineLength){
    // this ensures that the string doesn't go out of the box horizontally
    // this is not vertical paging
    fstream fout;
    fstream fin;
    string filename = "formattedfile.txt";
    fin.open(infile, ios::in);
    fout.open(filename, ios::out);
    while(!fin.eof()){
        string verylong, line;
        getline(fin, verylong);
        char firstChar = verylong[0];
        string name;
        if(firstChar == 'N'){
            name = "Niharika: ";
        }
        else if (firstChar == 'P'){
            name = "Prabal: ";
        }
        else{
            name = "";
        }
        while(verylong.length()>lineLength){
            line = verylong.substr(0,lineLength)+"\n";
            verylong = name+verylong.substr(lineLength,verylong.size()-1);
            fout<<line;
        }
        fout<<verylong+"\n";
    }
    fin.close();
    fout.close();
    return filename;
}

int main(int argc, char const *argv[])
{
    setlocale(LC_ALL, "");
    initscr();
    noecho();
    cbreak();
    curs_set(0);

    if(LINES < 30){
        string warningmsg = "Since paging is not yet implemented, reopen in fullscreen";
        string infomsg = "LINES at present: %d";
        string infomsg2 = "Required LINES at least 30";
        mvprintw(LINES/2, COLS/2 - (warningmsg.length()/2), warningmsg.c_str());
        mvprintw(LINES/2 +1, COLS/2 - (infomsg.length()/2), infomsg.c_str(), LINES);
        mvprintw(LINES/2 +2, COLS/2 - (infomsg2.length()/2), infomsg2.c_str());
        refresh();
        getch();
        endwin();
        exit(EXIT_FAILURE);
    }

    wborder(stdscr, '~', '~', '*', '*', '*', '*', '*', '*');
    string happymsg = "Happy Birthday Niharika!";
    mvprintw(LINES/2, COLS/2 - happymsg.length()/2, happymsg.c_str());
    string welcomemsg = "WELCOME to our memorylane";
    string welcomemsg2 = "There were nearly infinite messages to keep here, but I selected these few nice ones";
    string welcomemsg3 = "I hope you like it. Press a key to continue";
    mvprintw(LINES/2 + 1, COLS/2 - welcomemsg.length()/2, welcomemsg.c_str());
    mvprintw(LINES/2 + 2, COLS/2 - welcomemsg2.length()/2, welcomemsg2.c_str());
    mvprintw(LINES/2 + 3, COLS/2 - welcomemsg3.length()/2, welcomemsg3.c_str());
    refresh();
    getch();
    clear();

    mvprintw(LINES - 1, 0, "Navigate: up/down arrows\t\tQuit: q");
    refresh();

    lwin = newwin(LINES - 1, COLS / 4, 0, 0);
    int lmaxX = getmaxx(lwin);
    rwin = newwin(LINES - 1, (COLS - COLS / 4), 0, lmaxX);

    box(lwin, 0, 0);
    wrefresh(lwin);

    string infile = "realdump.txt";
    string filename = formattedfile(infile, (COLS - COLS / 4)-2);

    content = getmessage(filename);
    improveDateFormat(&content);

    Menu menu;
    for (int i = 0; i < content.size(); i++)
    {
        menu.items.push_back(content[i].datefield);
    }
    menu.highlightIndex = 2;
    menu.setIndex = 1;

    printmenu(lwin, &menu);

    string dateAndMessage = content[0].datefield + "\n" + content[0].message;
    rightWindowContent(rwin, dateAndMessage);

    int pressedKey = printMenuActions(lwin, &menu);
    while (pressedKey != 'q')
    {
        if (pressedKey == KEY_LEFT)
        {
            pressedKey = printMenuActions(lwin, &menu);
        }
        else if (pressedKey == KEY_RIGHT)
        {
            // pressedKey = rightWindowActions(rwin, &content, &content);
            pressedKey = printMenuActions(lwin, &menu);
        }
    }
    clear();
    string msg = "Bye";
    mvprintw(LINES / 2, ((COLS / 2) - (msg.size() / 2)), msg.c_str());
    getch();
    endwin();
    return 0;
}
