#include <iostream>
#include <ncurses.h>
#include <string>
#include <vector>
#include <fstream>
#include <locale.h>
using namespace std;

struct Menu
{
    vector<string> items;
    int highlightIndex;
    int setIndex;
};

// struct Content{
//     string text;
//     int lines;
// };

struct MessageContainer{
  string datefield;
  string message;
};

const string months[] = {
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
};

void printmenu(WINDOW *win, Menu *menu);
int printMenuActions(WINDOW *win, Menu *menu);
void rightWindowContent(WINDOW *win, string verylong);
vector<MessageContainer> getmessage(string filename);
void improveDateFormat(vector<MessageContainer>* content);
string formattedfile(string infile, int lineLength);
// int rightWindowActions(WINDOW *win, Content* above, Content* below);